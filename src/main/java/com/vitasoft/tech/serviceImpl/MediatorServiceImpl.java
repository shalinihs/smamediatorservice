package com.vitasoft.tech.serviceImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vitasoft.tech.entity.Mediator;
import com.vitasoft.tech.exception.MediatorNotFoundException;
import com.vitasoft.tech.repository.MediatorRepository;
import com.vitasoft.tech.service.IMediatorService;

@Service
public class MediatorServiceImpl implements IMediatorService {

	@Autowired
	MediatorRepository repo;

	@Override
	public Long createMediator(Mediator mediator) {

		return repo.save(mediator).getId();
	}

	@Override
	public Mediator getOneMediator(Long id) {
		Optional<Mediator> opt = repo.findById(id);
		if (!opt.isPresent()) {
			throw new MediatorNotFoundException("Mediator '" + id + "' Not found");

		} else {
			return opt.get();
		}

	}

	@Override
	public List<Mediator> getAllMediator() {

		return repo.findAll();
	}

}
