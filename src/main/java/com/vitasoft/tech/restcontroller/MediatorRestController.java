package com.vitasoft.tech.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vitasoft.tech.entity.Mediator;
import com.vitasoft.tech.exception.MediatorNotFoundException;
import com.vitasoft.tech.service.IMediatorService;

@RestController
@RequestMapping("/mediator")
public class MediatorRestController {

	@Autowired
	IMediatorService service;

//	1.Create Mediator
	@PostMapping("/create")
	public ResponseEntity<String> createMediator(@RequestBody Mediator mediator) {
		ResponseEntity<String> response = null;
		Long id = service.createMediator(mediator);
		response = ResponseEntity.ok("Mediator '" + id + "' Created");
		return response;

	}

//	2.Get One Mediator
	@GetMapping("/find/{id}")
	public ResponseEntity<Mediator> getOneMediator(@PathVariable Long id) {
		ResponseEntity<Mediator> response = null;
		try {
			Mediator medtr = service.getOneMediator(id);
			response = new ResponseEntity<Mediator>(medtr, HttpStatus.OK);

		} catch (MediatorNotFoundException mnfe) {
			mnfe.printStackTrace();
			throw mnfe;// global Exception Handler
		}
		return response;
	}

//	3.Get All Mediator
	@GetMapping("/find/all")
	public ResponseEntity<List<Mediator>> getAllMediator() {
		ResponseEntity<List<Mediator>> response = null;
		List<Mediator> list = service.getAllMediator();
		response = ResponseEntity.ok(list);
		return response;
	}

}
