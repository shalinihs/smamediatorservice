package com.vitasoft.tech.exception;

public class MediatorNotFoundException extends RuntimeException {
	
	public MediatorNotFoundException(){
		super();
	}
	public MediatorNotFoundException(String message){
		super(message);
	}

}
