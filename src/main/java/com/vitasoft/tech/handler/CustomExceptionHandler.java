package com.vitasoft.tech.handler;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.vitasoft.tech.exception.MediatorNotFoundException;
import com.vitasoft.tech.payload.response.ErrorMessage;

@RestControllerAdvice
public class CustomExceptionHandler {

	@ExceptionHandler(MediatorNotFoundException.class)
	public ResponseEntity<ErrorMessage> mediatorExceptionHandler(MediatorNotFoundException mnfe) {

		return ResponseEntity.internalServerError().body(new ErrorMessage(new Date().toString(), mnfe.getMessage(),
				HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR.name()));

	}

}
