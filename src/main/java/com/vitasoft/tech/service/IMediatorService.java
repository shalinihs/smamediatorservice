package com.vitasoft.tech.service;

import java.util.List;

import com.vitasoft.tech.entity.Mediator;

public interface IMediatorService {
	
	
	Long createMediator(Mediator mediator);
	Mediator getOneMediator(Long id);
	List<Mediator> getAllMediator();
	
	

}
