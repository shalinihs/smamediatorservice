package com.vitasoft.tech.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.vitasoft.tech.entity.Mediator;

public interface MediatorRepository extends JpaRepository<Mediator,Long> {

}
