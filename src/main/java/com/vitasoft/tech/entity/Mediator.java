package com.vitasoft.tech.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="mediator_tab")
public class Mediator {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="med_id_col")
	private Long id;
	
	@Column(name="med_name_col")
	private String name;
	
	@Column(name="med_email_col")
	private String email;
	
	@Column(name="med_regNum_col")
	private String registerNum;
	
	@Column(name="med_medcode_col")
	private String mediatorCode;
	
	@Column(name="med_addr_col")
	private String address;
	
	@Column(name="med_serviceMode_col")
	private String serviceMode;
	
	@Column(name="med_status_col")
	private String status;

}
